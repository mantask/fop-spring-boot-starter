package com.gitlab.spring.boot.starter.fop.configuration;

import java.io.InputStream;
import java.io.OutputStream;

public interface FopContext {

    InputStream getData();

    InputStream getSchema();

    String getMimeType();

    OutputStream getOutputStream();
}
