package com.gitlab.spring.boot.starter.fop.configuration;

import java.io.*;

final class FopContextImpl implements FopContext {
    public static final String DEFAULT_MIME_TYPE = "application/pdf";

    private final InputStream data;
    private final InputStream schema;
    private final String mimeType = DEFAULT_MIME_TYPE;
    private final OutputStream outputStream;

    public FopContextImpl(InputStream data,
                          InputStream schema,
                          OutputStream outputStream
    ) {
        this.data = data;
        this.schema = schema;
        this.outputStream = outputStream;
    }

    public FopContextImpl(String dataFileName,
                          String schemaFileName,
                          String outputFileName) throws FileNotFoundException {
        this.data = new FileInputStream(dataFileName);
        this.schema = new FileInputStream(schemaFileName);
        this.outputStream = new FileOutputStream(outputFileName);
    }

    public InputStream getData() {
        return data;
    }

    public InputStream getSchema() {
        return schema;
    }

    @Override
    public String getMimeType() {
        return mimeType;
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }
}
