package com.gitlab.spring.boot.starter.fop.configuration;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.springframework.util.Assert;

import javax.xml.transform.*;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.io.OutputStream;

public class SimpleFopWriter implements FopWriter {
    private final FopFactory fopFactory;
    private final TransformerFactory transformerFactory;

    public SimpleFopWriter(FopFactory fopFactory,
                           TransformerFactory transformerFactory
    ) {
        this.fopFactory = fopFactory;
        this.transformerFactory = transformerFactory;
    }

    public void write(FopContext fopContext) {
        try {
            writeInternal(fopContext);
        } catch (FOPException | TransformerException e) {
            throw new FopWriterException(e);
        }

    }

    private void writeInternal(FopContext fopContext) throws FOPException, TransformerException {
        Assert.notNull(fopContext);

        Fop fop = getFop(fopContext.getMimeType(), fopContext.getOutputStream());
        transform(fop, fopContext.getData(), fopContext.getSchema());
    }

    private void transform(Fop fop,
                           InputStream data,
                           InputStream schema
    ) throws TransformerException, FOPException {
        Assert.notNull(data);
        Assert.notNull(schema);

        Source xml = new StreamSource(data);
        Source xsl = new StreamSource(schema);
        Result sax = new SAXResult(fop.getDefaultHandler());

        Transformer transformer = transformerFactory.newTransformer(xsl);
        transformer.transform(xml, sax);
    }

    private Fop getFop(String mimeType,
                        OutputStream outputStream) throws FOPException {
        Assert.notNull(mimeType);
        Assert.notNull(outputStream);

        return fopFactory.newFop(mimeType, outputStream);
    }
}
