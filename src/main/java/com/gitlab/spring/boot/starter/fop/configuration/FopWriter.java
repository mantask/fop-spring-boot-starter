package com.gitlab.spring.boot.starter.fop.configuration;

/**
 * This class is part of spring-boot-starter-fop
 * Created by stefan on 07.04.16.
 */
public interface FopWriter {

    void write(FopContext fopContext);

}
