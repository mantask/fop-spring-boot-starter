package com.gitlab.spring.boot.starter.fop.configuration;

import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.FopFactoryBuilder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.xml.transform.TransformerFactory;
import java.net.URI;

@Configuration
@ComponentScan
public class ApacheFopAutoConfiguration {

    @ConditionalOnMissingBean(FopWriter.class)
    @Bean
    public FopWriter fopWriter(FopFactory fopFactory,
                               TransformerFactory transformerFactory
    ) {
        return new SimpleFopWriter(fopFactory, transformerFactory);
    }

    @ConditionalOnMissingBean(TransformerFactory.class)
    @Bean
    public TransformerFactory transformerFactory() {
        return TransformerFactory.newInstance();
    }

    @ConditionalOnMissingBean(FopFactory.class)
    @Bean
    public FopFactory fopFactory() {
        URI defaultBaseUri = URI.create(".");
        return new FopFactoryBuilder(defaultBaseUri).build();
    }


}
